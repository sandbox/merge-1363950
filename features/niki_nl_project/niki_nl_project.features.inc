<?php
/**
 * @file
 * niki_nl_project.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function niki_nl_project_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_filefield_paths().
 */
function niki_nl_project_default_filefield_paths() {
  $settings = array();

  // Exported node::project::field_brochure
  $settings['node::project::field_brochure'] = array(
    'type' => 'node::project',
    'field' => 'field_brochure',
    'filename' => array(
      'value' => 'brochure_[file:fid].[file:ffp-extension-original]',
      'tolower' => 1,
      'pathauto' => 0,
      'transliterate' => 1,
    ),
    'filepath' => array(
      'value' => 'original/projecten/brochures',
      'tolower' => 0,
      'pathauto' => 0,
      'transliterate' => 0,
    ),
    'active_updating' => '0',
  );
  // Exported node::project::field_plattegrond
  $settings['node::project::field_plattegrond'] = array(
    'type' => 'node::project',
    'field' => 'field_plattegrond',
    'filename' => array(
      'value' => 'plattegrond_[file:fid].[file:ffp-extension-original]',
      'tolower' => 1,
      'pathauto' => 0,
      'transliterate' => 1,
    ),
    'filepath' => array(
      'value' => 'original/projecten/plattegronden',
      'tolower' => 0,
      'pathauto' => 0,
      'transliterate' => 0,
    ),
    'active_updating' => '0',
  );
  // Exported node::project::field_project_afbeelding
  $settings['node::project::field_project_afbeelding'] = array(
    'type' => 'node::project',
    'field' => 'field_project_afbeelding',
    'filename' => array(
      'value' => 'project_[file:fid].[file:ffp-extension-original]',
      'tolower' => 1,
      'pathauto' => 0,
      'transliterate' => 1,
    ),
    'filepath' => array(
      'value' => 'original/projecten/afbeeldingen',
      'tolower' => 0,
      'pathauto' => 0,
      'transliterate' => 0,
    ),
    'active_updating' => '0',
  );
  // Exported node::project::field_project_logo
  $settings['node::project::field_project_logo'] = array(
    'type' => 'node::project',
    'field' => 'field_project_logo',
    'filename' => array(
      'value' => 'logo_[file:fid][file:ffp-extension-original]',
      'tolower' => 1,
      'pathauto' => 0,
      'transliterate' => 1,
    ),
    'filepath' => array(
      'value' => 'original/projecten/logos',
      'tolower' => 0,
      'pathauto' => 0,
      'transliterate' => 0,
    ),
    'active_updating' => '0',
  );

  return $settings;
}

/**
 * Implements hook_node_info().
 */
function niki_nl_project_node_info() {
  $items = array(
    'project' => array(
      'name' => t('Project'),
      'base' => 'node_content',
      'description' => t('Hier maak je nieuwe projecten mee aan.'),
      'has_title' => '1',
      'title_label' => t('Projectnaam'),
      'help' => '',
    ),
  );
  return $items;
}
