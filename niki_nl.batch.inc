<?php

/**
 * Form builder function to allow choice of which batch to run.
 */
function niki_nl_batch_process_form() {
  $form['description'] = array(
    '#type' => 'markup',
    '#markup' => t('Here you can manually perform actions which are normally performed in the background by the website (using cron).<br><br>The first option (sync projects from niki) will import and sync all projects. Any fields you changed will be overwritten by this action if they also exist in Niki.'),
  );
  $form['batch'] = array(
    '#type' => 'select',
    '#title' => 'Choose batch',
    '#options' => array(
      'batch_1' => t('Sync projects from niki'),
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Go',
  );
  // check if the user filled out the necessary credentials
  $login = variable_get('niki_wsdl_login');
  $pass = variable_get('niki_wsdl_pass');
  if (empty($login) or empty($pass)) {
    drupal_set_message(t("You haven't provided the necessary credentials in the <a href='/admin/config/services/niki'>niki configuration</a> yet. As a result, this form is disabled."));
    $form['submit']['#disabled'] = TRUE;
  }
  return $form;
}

/**
 * submit handler for niki_batch_process_form
 */
function niki_nl_batch_process_form_submit($form, &$form_state) {
  $function = 'niki_nl_batch_process_' . $form_state['values']['batch'];

  $_SESSION['http_request_count'] = 0; // reset counter for debug information.

  // Execute the function named batch_example_1 or batch_example_2.
  $batch = $function();
  batch_set($batch);
}

/**
 * Get all the projects from niki and put every node save/update in a batch operation
 */
function niki_nl_batch_process_batch_1() {
  $projects = niki_nl_get_projects();
  if (is_array($projects->project)) {
    $num_operations = count($projects->project);
    drupal_set_message(t('Creating an array of @num operations', array('@num' => $num_operations)));
  } 
  else {
    drupal_set_message(t('No projects found in niki.', 'error'));
    return FALSE;
  }
  $operations = array();
  // Set up an operations array with @num elements, each doing function
  // niki_batch_op_1.
  // Each operation in the operations array means at least one new HTTP request,
  // running Drupal from scratch to accomplish the operation. If the operation
  // returns with $context['finished'] != TRUE, then it will be called again.
  // In this example, $context['finished'] is always TRUE.
  $i = 1;
  foreach ($projects->project as $project) {
    // Each operation is an array consisting of
    // - the function to call
    // - An array of arguments to that function
    $operations[] = array('niki_nl_batch_op_1', array($project));
    $i++;
  }
  $batch = array(
    'operations' => $operations,
    'finished' => 'niki_nl_batch_finished',
  );
  return $batch;
}

/**
 * Syncing projects one at the time. This functionality is mirrored in niki_queue_process.
 */
function niki_nl_batch_op_1($project, &$context) {
  _niki_nl_init_client();
  // check to see if the project already exists in db by checking internalId
  $result_p = niki_nl_check_project_exists($project->projectReference->internalId);
  if (is_array($result_p) && is_numeric($result_p['nid'])) {
    $nid = niki_nl_project_save($project, $result_p['nid']);
    // Store some result for post-processing in the finished callback.
    $context['results'][] = 'Updating project, nid:  ' . $nid;
    // the $project also contains the woningtypes
    if (isset($project->houseType)) {
      $woningtypes = $project->houseType;
      if (is_object($woningtypes)) {
        $woningtype = $woningtypes; // ? @TODO checken
        // if there is only one woningtype
      } elseif (is_array($woningtypes)) {
        foreach ($woningtypes as $woningtype) {
          // @TODO niki_nl_check_woningtype_exists afmaken!
          //$result_wt = niki_nl_check_woningtype_exists($woningtype->projectReference->internalId);
          niki_nl_woningtype_save($woningtype, $result_wt['nid']);
        }
      }
    }
    // $project also holds woningen 
  } 
  else {
    $nid = niki_nl_project_save($project, $nid = NULL);
    $context['results'][] = 'Creating new project, nid: ' . $nid;
  }

  // Optional message displayed under the progressbar.
  $context['message'] = t('Loading project "@title"', array('@title' => $project->name));

  _niki_nl_batch_update_http_requests();
}

/**
 * Batch 'finished' callback used by both batch 1 and batch 2.
 */
function niki_nl_batch_finished($success, $results, $operations) {
  if ($success) {
    // Here we could do something meaningful with the results.
    // We just display the number of nodes we processed...
    drupal_set_message(t('@count results processed in @requests HTTP requests.', array('@count' => count($results), '@requests' => _niki_nl_batch_get_http_requests())));
    drupal_set_message(t('The final result was "%final"', array('%final' => end($results))));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args', array('@operation' => $error_operation[0], '@args' => print_r($error_operation[0], TRUE))));
  }
}

/**
 * Utility function to count the HTTP requests in a session variable.
 */
function _niki_nl_batch_update_http_requests() {
  $_SESSION['http_request_count']++;
}

function _niki_nl_batch_get_http_requests() {
  return !empty($_SESSION['http_request_count']) ? $_SESSION['http_request_count'] : 0;
}

