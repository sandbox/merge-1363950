SECURITY NOTE:
This module assumes that the data from Niki (soap) is safe. 
 
INSTALLATION:
Install just as any normal module and then goto the config page in settings -> services.
 
FEATURES:
See project page.
- batch
- queue on cron
- soap xml

ROADMAP:
 - import woningtypes
 - link woningtypes met projecten
 - import woningen
 - link woningen met woningtypen (?)
 - import overige(?) data
 - extend features with woningtypes + woningen
 - set cron max time

 - unpublish nodes that dissapear in Niki? (with notification?)
 - implement an override function (disable sync for certain projects)
 
 - create custom permissions for niki
 - replace imagefield with media module
 - refactor and use module_load_include('inc', 'niki', '/path') ?
 - stamgegevens opvragen via xml feeds van niki en dan die als selectielijst gebruiken voor bv projectStatus
 - some kind of status dashboard?