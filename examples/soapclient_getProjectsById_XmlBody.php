<?php
    include 'config.php';
    
    include '../lib/SoapClientSoapBodyXML.php';

    $client = new SoapClientSoapBodyXML( $wsdlUrl, $soapConfiguration );

    try
    {
        $soapBodyXml = $client->__soapCall('getProjectsById', array($projectReference));
        print_r($soapBodyXml);
    }
    catch (SoapFault $e)
    {
        echo "EXCEPTION!\n";
        print_r($e);
        echo "END OF EXCEPTION!\n";
    }
?>
