<?php
    include 'config.php';
    
    include '../lib/WsseEnabledSoapClient.php';

    $client = new WsseEnabledSoapClient( $wsdlUrl, $soapConfiguration );

    try
    {
	    $result = $client->__soapCall('getFile', array(array('id'=>$exampleFileId)));
	    header("Content-Disposition: inline; filename=".$result->file->filename);
	    header("Content-Type: ".$result->file->mimetype);
        print($result->file->data);
    }
    catch (SoapFault $e)
    {
       print_r("Er is een fout opgetreden");
    }

?>
