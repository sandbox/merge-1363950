<?php
    include 'config.php';
    include '../lib/WsseEnabledSoapClient.php';

    $client = new WsseEnabledSoapClient( $wsdlUrl, $soapConfiguration );

    try
    {
        $result = $client->__soapCall(   'getProjectsById', array($projectReference));

        print "Results:\n";
        print_r($result);
        echo 'End of Results.';
    }
    catch (SoapFault $e)
    {
        echo "EXCEPTION!\n";
        print_r($e);
        echo "END OF EXCEPTION!\n";
    }

?>
