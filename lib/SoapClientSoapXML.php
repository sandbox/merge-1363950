<?php
    /**
    * This class extends the WsseEnabledSoapClient and instead of PHP Objects it returns
    * the Soap webservice XML.                                                                                      
    */

    include 'WsseEnabledSoapClient.php';

    class SoapClientSoapXML extends WsseEnabledSoapClient
    {
        var $rawSoapResponse;

        public function __soapCall( $function_name,
                                    $arguments,
                                    $options = NULL,
                                    $input_headers = NULL,
                                    $output_headers = NULL )
        {
            //Ignore php object results. Just call function to intercept XML in __doRequest
            parent::__soapCall(  $function_name,
                                 $arguments,
                                 $options,
                                 $input_headers,
                                 $output_headers );

            //Return intercepted XML response
            return $this->rawSoapResponse;
        }

        public function __doRequest($request,
                                    $location,
                                    $action,
                                    $version,
                                    $one_way=0 )
        {
            //Do original action
            $this->rawSoapResponse = parent::__doRequest(  $request,
                                                            $location,
                                                            $action,
                                                            $version,
                                                            $one_way);

            return $this->rawSoapResponse;
        }
    }
?>