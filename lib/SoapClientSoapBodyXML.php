<?php
    /**
    * This class extends the WsseEnabledSoapClient and instead of PHP Objects it returns
    * the Soap webservice XML.
    */

    include 'SoapClientSoapXML.php';

    class SoapClientSoapBodyXML extends SoapClientSoapXML
    {

        public function __soapCall( $function_name,
                                    $arguments,
                                    $options = NULL,
                                    $input_headers = NULL,
                                    $output_headers = NULL )
        {



            $soapXml = parent::__soapCall(  $function_name,
                                             $arguments,
                                             $options,
                                             $input_headers,
                                             $output_headers );

          $sXml = simplexml_load_string($soapXml);

    
      return '<?xml version="1.0"?>'.$sXml->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://project.service.projectservices.lnp.fundament.nl/')->getProjectsByIdResponse->asXML();

/*

            //Alternative is using DOMDocument (if available)
            /*
            $doc = new DOMDocument();
            $doc->loadXML($soapXml);

            //Select the soap Body element
            $bodyNodeList = $doc->getElementsByTagNameNS('http://schemas.xmlsoap.org/soap/envelope/','Body');

            //Create a new XML Document
            $doc = new DOMDocument();
            foreach ($bodyNodeList as $bodyDomElement)
            {
                foreach ($bodyDomElement->childNodes as $bodyChildDomElement)
                {
                    //Import and attach child nodes from Soap/Body element to new XML Document
                    $domNode = $doc->importNode($bodyChildDomElement, true);
                    $doc->appendChild($domNode);
                }
            }

            //Don't return the DOMDocument, but return a string XML version.
            $doc->formatOutput = true;
            return $doc->saveXML();
            */
        }
    }
?>