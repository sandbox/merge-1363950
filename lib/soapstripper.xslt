<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"
                xmlns:P="http://project.service.projectservices.lnp.fundament.nl/">
    <xsl:template match="text()|@*" mode="head"/>
    <xsl:template match="text()|@*" mode="body"/>
    <xsl:template match="text()|@*"/>
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
    <xsl:template match="/">
        <xsl:copy-of select="/S:Envelope/S:Body/P:getProjectsByIdResponse"/>
    </xsl:template>
</xsl:stylesheet>
