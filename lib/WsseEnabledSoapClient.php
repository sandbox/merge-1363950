<?php
    /**
     * This class extends the default SoapClient from PHP.
     * It appends security header extensions which are not supported by default.
     * Also strips leading and trailing white spaces from received XML to avoid PHP crashing on it.
     */

    class WsseEnabledSoapClient extends SoapClient
    {
        protected $soapSecurityHeader = NULL;

        function __construct($wsdlUrl, $options = array())
        {
            parent::__construct($wsdlUrl, $options);

            //Write the security header extension yourself, in lack of build in support
            $authenticationHeader = sprintf('<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                                                <wsse:UsernameToken>
                                                    <wsse:Username>%s</wsse:Username>
                                                    <wsse:Password>%s</wsse:Password>
                                                </wsse:UsernameToken>
                                            </wsse:Security>',
                                            htmlspecialchars($options['login']),
                                            htmlspecialchars($options['password']));

            $this->soapSecurityHeader = new SoapHeader("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security", new SoapVar($authenticationHeader, XSD_ANYXML));
        }

        public function __soapCall( $function_name,
                                    $arguments,
                                    $options = NULL,
                                    $input_headers = NULL,
                                    $output_headers = NULL )
        {
            //Combine our custom headers with optional user supplied headers.
            if (is_array($input_headers))
            {
                array_push($input_headers, $this->soapSecurityHeader);
            }
            else
            {
                $input_headers = $this->soapSecurityHeader;
            }

            return parent::__soapCall(  $function_name,
                                         $arguments,
                                         $options,
                                         $input_headers,
                                         $output_headers );
        }


        public function removeLeadingAndTrailingWhiteSpacesFromXml($xmlString)
        {
            //Strip leading whitespaces from XML response
            $start  = strpos($xmlString,'<?xml');
            $end    = strrpos($xmlString,'>');
            return substr($xmlString, $start, $end-$start+1);

        }

        /**
         * __doRequest() overrides the standard SoapClient to handle a local request
         */
        public function __doRequest($request,
                                    $location,
                                    $action,
                                    $version,
                                    $one_way=0 )
        {
            //Do original action
            $rawReceivedResponse = parent::__doRequest( $request,
                                                        $location,
                                                        $action,
                                                        $version,
                                                        $one_way);

            return $this->removeLeadingAndTrailingWhiteSpacesFromXml($rawReceivedResponse);
        }

    }
?>